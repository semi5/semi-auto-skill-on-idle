export function setup(ctx) {
    const id = 'semi-auto-skill-on-idle';
    const title = 'SEMI Auto Skill on Idle';
    const desc = 'If the SEMI detects you do not have a currently active skill, it will start doing the skill you select.';

    const DROPDOWN_CHOICES = [
        {
            value:'melvorD:Woodcutting',
            display:'Woodcutting',
        },
        {
            value:'melvorD:Fishing',
            display:'Fishing',
        },
        {
            value:'melvorD:Mining',
            display:'Mining',
        },
        {
            value:'melvorD:Agility',
            display:'Agility',
        },
        {
            value:'melvorD:Astrology',
            display:'Astrology',
        },
    ]

    // TODO: Make the action selectable
    const generateSelectedSkillOptions = (selectedSkill) => {
        return
    }

    const getCurrentlySelectedSkillID = () => mod.api.SEMI.getSubmodSettings(title, 'skill-selection')

    // Setup the settings menu
    mod.api.SEMI.addSubmodSettings(title, {
        label:'Idle Skill Selection',
        name:'skill-selection',
        hint:'If the SEMI detects you do not have a currently active skill, it will start doing the skill you select. May one day expand to crafting skills.',
        type:'dropdown',
        options:DROPDOWN_CHOICES,
        default:'melvorD:Agility',
    });

    ctx.onCharacterLoaded(ctx => {
        ctx.patch(GatheringSkill, 'stop').after(function(result) {
            // Check if we are actually idle of if we just stopped doing one action
            if(game.activeAction === undefined) {
                mod.api.SEMI.log(id, 'Switching to on Idle Skill')
                const selectedSkillID = getCurrentlySelectedSkillID()

                // Is the skill unlocked?
                if(!game.skills.getObjectByID(selectedSkillID).isUnlocked) {
                    return
                }

                switch(selectedSkillID) {
                    case 'melvorD:Woodcutting':
                        game.woodcutting.actions.allObjects.reverse().filter(x => x.level <= game.woodcutting.level).slice(0,game.woodcutting.treeCutLimit).forEach((tree) => {
                            game.woodcutting.selectTree(tree)
                        })
                        break

                    case 'melvorD:Fishing':
                        // Find our fish and area
                        let fish = game.fishing.actions.allObjects.reverse().filter(fishAction => fishAction.level <= game.fishing.level)[0]
                        let area = game.fishing.areas.allObjects.filter(area => area.fish.includes(fish))[0]

                        if(area) {
                            game.fishing.onAreaFishSelection(area, fish)
                            game.fishing.onAreaStartButtonClick(area)
                        }
                        break

                    case 'melvorD:Mining':
                        let ore = game.mining.actions.allObjects.reverse().filter(x => x.type === "Ore").filter(x => x.level <= game.mining.level)[0]
                        game.mining.onRockClick(ore)
                        break

                    case 'melvorD:Agility':
                        game.agility.start()
                        break

                    case 'melvorD:Astrology':
                        let constellation = game.astrology.actions.allObjects.reverse().filter(x => x.level <= game.astrology.level)[0]
                        game.astrology.studyConstellationOnClick(constellation)
                        break

                    default:
                        break
                }
            }
        })
    })

    mod.api.SEMI.log(id, 'Successfully loaded!')
}
